package Ciberbomb;
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import java.awt.*;
import robocode.*;

 
public class CiberBomb extends Robot
{
	
		boolean peek;
	double moveA;
	

	public void run() {
	
		setBodyColor(Color.blue);
		setGunColor(Color.black);
		setRadarColor(Color.orange);
		setBulletColor(Color.red);
		setScanColor(Color.cyan);
		
	moveA = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
	
		turnLeft(getHeading() % 90);
		ahead(moveA);
	
		peek = true;
		turnGunRight(90);
		turnRight(90);

		while (true) {
		
			peek = true;
		
			ahead(moveA);
		
			peek = false;
	
			turnRight(90);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
	
		fire(5);
	}



	public void onHitByBullet(HitByBulletEvent e) {
	
		back(20);
	}
	
	public void onHitWall(HitWallEvent e) {
	
		back(20);
	}	
}
	

